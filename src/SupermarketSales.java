import java.awt.BorderLayout;
import java.sql.*;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.awt.Color;
import java.awt.Component;
import java.awt.Desktop;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.Font;
import java.awt.GraphicsDevice;
import java.awt.GraphicsEnvironment;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.GridLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.FocusEvent;
import java.awt.event.FocusListener;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.io.File;

import javax.swing.BoxLayout;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTabbedPane;
import javax.swing.JTable;
import javax.swing.JTextArea;
import javax.swing.JTextField;
import javax.swing.SwingConstants;
import javax.swing.SwingUtilities;
import javax.swing.UIManager;
import javax.swing.table.DefaultTableModel;

public class SupermarketSales extends JFrame {

	// Objects

	MouseHandler mouseHandler = new MouseHandler();
	ActionHandler actionHandler = new ActionHandler();
	FocusHandler focusHandler = new FocusHandler();
	GridBagLayout gb = new GridBagLayout();
	GridBagConstraints gbc = new GridBagConstraints();
	GridBagConstraints gbcadd = new GridBagConstraints();
	GridBagConstraints gbcedit = new GridBagConstraints();
	GridBagConstraints gbceditWith = new GridBagConstraints();
	GridBagConstraints gbcsell = new GridBagConstraints();

	// Variables

	int pX = 0, pY = 0;
	int rowc;
	int counter;
	String[] catagories = { "Food and Beverage", "Electronics", "Clothings", "Other" };
	String branchName;

	String host = "NHATTY";
	String port = "3306";
	String pass = "mikiyas123";
	String dbname = "supermarket";
	String user = "root";
	String url = "jdbc:mysql://" + host + ":" + port + "/" + dbname + "?useSSL=false";

	// Images

	ImageIcon icon = new ImageIcon(getClass().getResource("Asset 7.png"));
	ImageIcon minIcon = new ImageIcon(getClass().getResource("Asset 8.png"));
	ImageIcon maxIcon = new ImageIcon(getClass().getResource("Asset 9.png"));
	ImageIcon closeIcon = new ImageIcon(getClass().getResource("Asset 10.png"));
	ImageIcon logoIcon = new ImageIcon(getClass().getResource("Asset 1.png"));
	ImageIcon homeIcon = new ImageIcon(getClass().getResource("Asset 6.png"));
	ImageIcon reportIcon = new ImageIcon(getClass().getResource("Asset 3.png"));
	ImageIcon aboutIcon = new ImageIcon(getClass().getResource("Asset 4.png"));

	// Colors and Fonts

	Font font = new Font("Montserrat", Font.BOLD, 12);
	Font fontPlain = new Font("Montserrat", Font.PLAIN, 12);
	Color blue = new Color(204, 251, 246);
	Color white = new Color(255, 255, 255);
	Color fade = new Color(235, 253, 251);

	// Panels

	JPanel mainPanel = new JPanel(new BorderLayout());
	JPanel head = new JPanel(new BorderLayout());
	JPanel headTools = new JPanel(new GridLayout());

	JPanel sidePanel = new JPanel(new BorderLayout());
	JPanel logoPanel = new JPanel(new BorderLayout());
	JPanel menuPanel = new JPanel(new GridLayout(6, 1, 10, 10));
	JPanel homeButtonPanel = new JPanel(new GridLayout(1, 3, 2, 2));
	JPanel reportButtonPanel = new JPanel(new GridLayout(1, 3, 2, 2));
	JPanel aboutButtonPanel = new JPanel(new GridLayout(1, 3, 2, 2));

	JPanel centerPanel = new JPanel(new GridBagLayout());

	JPanel homePanel = new JPanel(new BorderLayout());
	JTabbedPane homeTab = new JTabbedPane();

	JPanel sellPanel = new JPanel(new GridBagLayout());
	JPanel sellSearchPanel = new JPanel(new BorderLayout());
	JPanel sellSearchTf = new JPanel(new BorderLayout());
	JPanel sellTable = new JPanel(new GridBagLayout());
	JPanel sellPanelWithButton = new JPanel();

	JPanel reportPanel = new JPanel(new BorderLayout());
	JTabbedPane reportTab = new JTabbedPane();
	JPanel reportP = new JPanel();
	JPanel availablePanel = new JPanel();
	JPanel soldPanel = new JPanel();

	JPanel aboutPanel = new JPanel(new BorderLayout());
	JTabbedPane aboutTab = new JTabbedPane();
	JPanel aboutP = new JPanel();

	// Labels

	JLabel programIcon = new JLabel(icon);
	JLabel logo = new JLabel(logoIcon);
	JLabel homeIconLabel = new JLabel(homeIcon);
	JLabel reportIconLabel = new JLabel(reportIcon);
	JLabel aboutIconLabel = new JLabel(aboutIcon);
	JLabel sellNameOfTheProduct = new JLabel("Name Of The Product");
	JLabel sellCatagory = new JLabel("Category");
	JLabel sellPriceOfTheProduct = new JLabel("Price Of The Product");
	JLabel sellQuantity = new JLabel("Quantity");
	JLabel sellDiscription = new JLabel("Discription");
	JLabel sellPriceValidation = new JLabel();
	JLabel sellQuantityValidation = new JLabel();

	// JTextFields

	JTextField sellNameTf = new JTextField("Enter The Name Of The Product", 30);
	JTextField sellPriceTf = new JTextField("Enter The Price", 30);
	JTextField sellQuantityTf = new JTextField("Enter The Quantity", 30);
	JTextField sellTf = new JTextField("Search", 66);

	// JComboBox

	JComboBox sellCatagoryBox = new JComboBox(catagories);
	JTextArea sellDiscriptionArea = new JTextArea(3, 30);

	// Buttons

	JButton min = new JButton(minIcon);
	JButton max = new JButton(maxIcon);
	JButton close = new JButton(closeIcon);
	JButton home = new JButton("Home");
	JButton report = new JButton("Report");
	JButton about = new JButton("About");
	JButton seller = new JButton("Sell");
	JButton refresh = new JButton("Refresh");

	Connection connection;
	Statement statement;
	ResultSet resultSet;
	Statement statement2 = null;
	ResultSet resultSet2;
	Statement statement3;
	Statement statement4;

	// Table

	JTable table = new JTable();
	DefaultTableModel tableModel = new DefaultTableModel(new Object[0][7],
			new String[] { "ID", "Name", "Catagory", "Price", "Quantity", "Branch", "Discription" });
	JTable reportTable = new JTable();
	DefaultTableModel reportTableModel = new DefaultTableModel(new Object[0][7],
			new String[] { "ID", "Name", "Catagory", "Price", "Quantity", "Branch", "Discription" });
	JTable soldTable = new JTable();
	DefaultTableModel soldTableModel = new DefaultTableModel(new Object[0][7],
			new String[] { "ID", "Name", "Catagory", "Price", "Quantity", "Branch", "Discription" });

	// Methods

	private void maximize() {
		// Get GraphicsEnvironment object for getting GraphicsDevice object
		GraphicsEnvironment env = GraphicsEnvironment.getLocalGraphicsEnvironment();
		// Get the screen devices
		GraphicsDevice[] g = env.getScreenDevices();
		// I only have one, the first one
		// If current window is full screen, set fullscreen window to null
		// else set the current screen
		g[0].setFullScreenWindow(g[0].getFullScreenWindow() == this ? null : this);
	}

	private class MouseHandler implements MouseListener {

		@Override
		public void mouseClicked(MouseEvent arg0) {
			// TODO Auto-generated method stub
		}

		@Override
		public void mouseEntered(MouseEvent event) {
			if (event.getComponent() == homeButtonPanel || event.getSource() == home) {
				homeButtonPanel.setBackground(fade);
			} else if (event.getComponent() == reportButtonPanel || event.getSource() == report) {
				reportButtonPanel.setBackground(fade);
			} else if (event.getComponent() == aboutButtonPanel || event.getSource() == about) {
				aboutButtonPanel.setBackground(fade);
			}
		}

		@Override
		public void mouseExited(MouseEvent event) {
			homeButtonPanel.setBackground(blue);
			reportButtonPanel.setBackground(blue);
			aboutButtonPanel.setBackground(blue);

		}

		@Override
		public void mousePressed(MouseEvent arg0) {
			// TODO Auto-generated method stub

		}

		@Override
		public void mouseReleased(MouseEvent arg0) {
			// TODO Auto-generated method stub

		}

	}

	public class ActionHandler implements ActionListener {
		@Override
		public void actionPerformed(ActionEvent ae) {
			if (ae.getSource() == home) {
				homePanel.setVisible(true);
				reportPanel.setVisible(false);
				aboutPanel.setVisible(false);
				if (homePanel.isVisible()) {
					homeButtonPanel.setBackground(Color.white);
				}
			} else if (ae.getSource() == report) {
				homePanel.setVisible(false);
				reportPanel.setVisible(true);
				aboutPanel.setVisible(false);
				if (reportPanel.isVisible()) {
					reportButtonPanel.setBackground(white);
				}
			} else if (ae.getSource() == about) {
				homePanel.setVisible(false);
				reportPanel.setVisible(false);
				aboutPanel.setVisible(true);
				if (aboutPanel.isVisible()) {
					aboutButtonPanel.setBackground(white);
				}
			} else if (ae.getSource() == seller) {

				String productBefore = "";
				double p;
				int q;

				int i;
				int quanGain = (int) Integer.parseInt(JOptionPane.showInputDialog("Enter the Quantity Of Product: "));

				String[] data = new String[3];

				int x = table.getSelectedRow();
				String nameProduct = table.getValueAt(x, 1).toString();
				int quanBefore = Integer.parseInt(table.getValueAt(x, 4).toString());
				int quanBefore2 = Integer.parseInt(table.getValueAt(x, 4).toString());
				data[2] = sellQuantityTf.getText();

				int y = table.getRowCount();
				try {
					connection = DriverManager.getConnection(url, user, pass);
					statement = connection.createStatement();
					statement2 = connection.createStatement();
					statement3 = connection.createStatement();
					statement4 = connection.createStatement();
					resultSet = statement
							.executeQuery("select quantity from store where productName = \"" + data[0] + "\"");
					while (resultSet.next()) {
						quanBefore = resultSet.getInt("quantity");
					}

					statement.execute("update store set quantity= " + (quanBefore - quanGain) + " WHERE id=\""
							+ table.getValueAt(x, 0) + "\"");

					resultSet2 = statement2.executeQuery("select productName,quantity from sold");
					while (resultSet2.next()) {
						productBefore = resultSet2.getString(1);
						quanBefore2 = resultSet2.getInt(2);
						if (nameProduct.equals(productBefore)) {
							statement3.execute("update sold set quantity= " + (quanBefore2 + quanGain) + " WHERE id="
									+ table.getValueAt(x, 0));
						}
					}
					try {
						if (!(nameProduct.equals(productBefore))) {
							p = Double.parseDouble(table.getValueAt(x, 3).toString());
							q = quanGain;
							statement4.execute("insert into sold values(" + table.getValueAt(x, 0) + ",\""
									+ table.getValueAt(x, 1) + "\",\"" + table.getValueAt(x, 2) + "\"," + p + "," + q
									+ ",\"" + table.getValueAt(x, 5) + "\",\"" + table.getValueAt(x, 6) + "\")");
						}
					} catch (Exception e) {
					}

					if ((quanBefore - quanGain) < 1) {
						tableModel.removeRow(x);
					}

				} catch (SQLException ex) {
					ex.printStackTrace();

				} finally {
					try {
						statement.close();
						connection.close();
					} catch (SQLException v) {
					}
				}
				for (int j = counter; j >= 0; j--) {
					try {
						tableModel.removeRow(j);
						reportTableModel.removeRow(j);
						soldTableModel.removeRow(j);
					} catch (Exception e) {
					}
				}
				loadData();
				loadDataToSold();

			} else if (ae.getSource() == refresh) {
				for (int j = counter; j >= 0; j--) {
					try {
						tableModel.removeRow(j);
						reportTableModel.removeRow(j);
						soldTableModel.removeRow(j);
					} catch (Exception e) {
					}
				}
				loadData();
				loadDataToSold();
			}
		}
	}

	private class FocusHandler implements FocusListener {
		@Override
		public void focusGained(FocusEvent fo) {
			fo.getComponent().setForeground(Color.black);
			if (fo.getSource() == sellNameTf) {
				if (sellNameTf.getText().equals("Enter The Name Of The Product")) {
					sellNameTf.setText("");
				}
			} else if (fo.getSource() == sellPriceTf) {
				if (sellPriceTf.getText().equals("Enter The Price")) {
					sellPriceTf.setText("");
				}
			} else if (fo.getSource() == sellQuantityTf) {
				if (sellQuantityTf.getText().equals("Enter The Quantity")) {
					sellQuantityTf.setText("");
				}
			} else if (fo.getSource() == sellTf) {
				if (sellTf.getText().equals("Search")) {
					sellTf.setText("");
				}
			}
		}

		@Override
		public void focusLost(FocusEvent fe) {
			// fe.getComponent().setForeground(Color.LIGHT_GRAY);
			// if (fe.getSource() == nameTf) {
			// nameTf.setText("Enter The Name Of The Product");
			// } else if (fe.getSource() == priceTf) {
			// priceTf.setText("Enter The Price");
			// } else if (fe.getSource() == quantityTf) {
			// quantityTf.setText("Enter The Quantity");
			// } else if (fe.getSource() == sellTf) {
			// editTf.setText("Search");
			// }
		}
	}

	private void addComponentToSellSearch(Component component, int x, int y) {
		gbceditWith.insets = new Insets(0, 0, 0, 0);
		gbceditWith.gridx = x;
		gbceditWith.gridy = y;
		gbceditWith.weightx = 1;
		gbceditWith.fill = GridBagConstraints.NONE;
		gbceditWith.anchor = GridBagConstraints.NORTHWEST;
		sellSearchTf.setLayout(gb);
		sellSearchTf.add(component, gbceditWith);
	}

	public void loadDataToSold() {
		try {
			connection = DriverManager.getConnection(url, user, pass);
			statement = connection.createStatement();
			resultSet = statement.executeQuery("select* from sold where branch = '" + branchName + "'");
			int i = 1;
			String[] row;
			row = new String[7];
			while (resultSet.next()) {
				row[0] = Integer.toString(resultSet.getInt(1));
				row[1] = resultSet.getString(2);
				row[2] = resultSet.getString(3);
				row[3] = Float.toString(resultSet.getFloat(4));
				row[4] = Integer.toString(resultSet.getInt(5));
				row[5] = resultSet.getString(6);
				row[6] = resultSet.getString(7);

				soldTableModel.addRow(row);
				i = resultSet.getInt(1);
			}
			counter = i + 1;

		} catch (SQLException ex) {
			Logger.getLogger(SupermarketSales.class.getName()).log(Level.SEVERE, null, ex);
		} finally {
			try {
				statement.close();
				connection.close();
				resultSet.close();
			} catch (SQLException v) {
			}
		}
	}

	public SupermarketSales(String bName) {

		branchName = bName;
		setSize(1200, 630);
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setUndecorated(true);
		setLocationRelativeTo(null);

		loadData();
		loadDataToSold();

		// Frame look and Feel

		UIManager.LookAndFeelInfo[] looks = UIManager.getInstalledLookAndFeels();
		try {
			UIManager.setLookAndFeel(looks[1].getClassName());
			SwingUtilities.updateComponentTreeUI(homeTab);
			SwingUtilities.updateComponentTreeUI(reportTab);
			SwingUtilities.updateComponentTreeUI(aboutTab);
			SwingUtilities.updateComponentTreeUI(sellNameTf);
			SwingUtilities.updateComponentTreeUI(sellCatagoryBox);
			SwingUtilities.updateComponentTreeUI(sellQuantityTf);
			SwingUtilities.updateComponentTreeUI(seller);
			SwingUtilities.updateComponentTreeUI(sellTf);
			SwingUtilities.updateComponentTreeUI(refresh);

		} catch (Exception ex) {
			ex.printStackTrace();
		}

		setIconImage(icon.getImage());

		// headTools

		headTools.add(min);
		headTools.add(max);
		headTools.add(close);
		headTools.setBackground(blue);

		min.setFocusPainted(false);
		min.setContentAreaFilled(false);
		min.setBorderPainted(false);
		max.setFocusPainted(false);
		max.setContentAreaFilled(false);
		max.setBorderPainted(false);
		close.setFocusPainted(false);
		close.setContentAreaFilled(false);
		close.setBorderPainted(false);

		// head

		head.add(programIcon, BorderLayout.WEST);
		head.add(headTools, BorderLayout.EAST);
		head.setBackground(blue);

		// head ActionListener

		min.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent ae) {
				// minimize
				setState(ICONIFIED);
			}
		});
		max.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent ae) {
				maximize();
			}
		});
		close.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent ae) {
				// terminate program
				System.exit(0);
			}
		});
		head.addMouseListener(new MouseAdapter() {
			public void mousePressed(MouseEvent me) {
				// Get x,y and store them
				pX = me.getX();
				pY = me.getY();
			}
		});
		// Add MouseMotionListener for detecting drag
		head.addMouseMotionListener(new MouseAdapter() {
			public void mouseDragged(MouseEvent me) {
				// Set the location
				// get the current location x-co-ordinate and then get
				// the current drag x co-ordinate, add them and subtract most recent
				// mouse pressed x co-ordinate
				// do same for y co-ordinate
				setLocation(getLocation().x + me.getX() - pX, getLocation().y + me.getY() - pY);
			}
		});

		// menuPanel

		homeButtonPanel.setBackground(blue);
		homeButtonPanel.add(homeIconLabel);
		homeButtonPanel.add(home);
		home.setFont(font);
		home.setFocusPainted(false);
		home.setBorderPainted(false);
		home.setContentAreaFilled(false);
		home.setHorizontalAlignment(SwingConstants.LEFT);
		homeButtonPanel.addMouseListener(mouseHandler);
		home.addMouseListener(mouseHandler);
		home.addActionListener(actionHandler);

		reportButtonPanel.setBackground(blue);
		reportButtonPanel.add(reportIconLabel);
		reportButtonPanel.add(report);
		report.setFont(font);
		report.setFocusPainted(false);
		report.setBorderPainted(false);
		report.setContentAreaFilled(false);
		report.setHorizontalAlignment(SwingConstants.LEFT);
		reportButtonPanel.addMouseListener(mouseHandler);
		report.addMouseListener(mouseHandler);
		report.addActionListener(actionHandler);

		aboutButtonPanel.setBackground(blue);
		aboutButtonPanel.add(aboutIconLabel);
		aboutButtonPanel.add(about);
		about.setFont(font);
		about.setFocusPainted(false);
		about.setBorderPainted(false);
		about.setContentAreaFilled(false);
		about.setHorizontalAlignment(SwingConstants.LEFT);
		aboutButtonPanel.addMouseListener(mouseHandler);
		about.addMouseListener(mouseHandler);
		about.addActionListener(actionHandler);

		menuPanel.setBackground(blue);
		menuPanel.add(homeButtonPanel);
		menuPanel.add(reportButtonPanel);
		menuPanel.add(aboutButtonPanel);

		// sidePanel

		sidePanel.setBackground(blue);
		sidePanel.setPreferredSize(new Dimension(250, 500));
		logoPanel.setPreferredSize(new Dimension(250, 130));
		logoPanel.setBackground(blue);
		logoPanel.add(logo);
		sidePanel.add(logoPanel, BorderLayout.NORTH);
		sidePanel.add(menuPanel, BorderLayout.CENTER);

		// centerPanel

		gbc.gridx = 2;
		gbc.gridy = 0;
		gbc.anchor = GridBagConstraints.NORTH;
		gbc.weightx = 250;
		gbc.weighty = 500;
		gbc.fill = GridBagConstraints.BOTH;
		gbc.ipadx = 750;
		gbc.ipady = 500;
		centerPanel.setBackground(white);

		// homePanel

		seller.addActionListener(actionHandler);
		sellTf.addKeyListener(new KeyListener() {

			@Override
			public void keyTyped(KeyEvent arg0) {
				// TODO Auto-generated method stub
				SwingUtilities.invokeLater(new Runnable() {
					@Override
					public void run() {
						try {
							Thread.sleep(20);
						} catch (Exception e) {

						}
						String word = sellTf.getText();
						search(word);
					}
				});
			}

			@Override
			public void keyReleased(KeyEvent arg0) {
				// TODO Auto-generated method stub
			}

			@Override
			public void keyPressed(KeyEvent arg0) {
				// TODO Auto-generated method stub
			}
		});
		table.setFillsViewportHeight(true);
		table.setAutoCreateRowSorter(true);

		gbcedit.fill = GridBagConstraints.BOTH;
		gbcedit.weightx = 1;
		gbcedit.weighty = 1;

		sellTf.setForeground(Color.LIGHT_GRAY);
		sellTf.addFocusListener(focusHandler);

		table.setModel(tableModel);
		table.setAutoCreateRowSorter(true);
		table.setBackground(white);
		sellSearchPanel.setBackground(white);
		sellTable.setBackground(white);
		sellTable.add(new JScrollPane(table), gbcedit);

		addComponentToSellSearch(sellTf, 1, 1);
		addComponentToSellSearch(seller, 2, 1);
		addComponentToSellSearch(refresh, 3, 1);
		sellTf.addActionListener(actionHandler);
		refresh.addActionListener(actionHandler);

		sellSearchPanel.add(sellSearchTf, BorderLayout.NORTH);
		sellSearchPanel.add(sellTable);
		sellSearchPanel.setMinimumSize(new Dimension(750, 500));
		sellPanel.add(sellSearchPanel, gbc);

		homePanel.add(sellPanel);
		homePanel.setVisible(true);
		centerPanel.add(homePanel, gbc);

		// reportPanel

		availablePanel.setBackground(white);
		availablePanel.setLayout(gb);
		soldPanel.setLayout(gb);
		reportTab.addTab("Available Item", null, availablePanel, "report");
		reportTab.addTab("Sold Items", null, soldPanel, "report");
		reportPanel.add(reportTab);
		reportPanel.setVisible(false);
		reportTable.setModel(reportTableModel);
		soldTable.setModel(soldTableModel);
		reportTable.setAutoCreateRowSorter(true);
		availablePanel.add(new JScrollPane(reportTable), gbcedit);
		reportTable.setBackground(white);
		soldTable.setAutoCreateRowSorter(true);
		soldPanel.add(new JScrollPane(soldTable), gbcedit);
		soldTable.setBackground(white);
		centerPanel.add(reportPanel, gbc);

		// aboutPanel

		aboutP.setBackground(white);
		aboutTab.addTab("About Us", null, aboutP, "About Us");
		aboutPanel.add(aboutTab);
		aboutPanel.setVisible(false);
		JLabel about = new JLabel("Java Group Work");
		about.setFont(font);
		aboutP.add(about);
		centerPanel.add(aboutPanel, gbc);

		// mainPanel

		mainPanel.add(centerPanel, BorderLayout.CENTER);
		mainPanel.add(sidePanel, BorderLayout.WEST);

		// AddToTheMainFrame

		add(head, BorderLayout.NORTH);
		add(mainPanel, BorderLayout.CENTER);
	}

	public void loadData() {
		try {
			connection = DriverManager.getConnection(url, user, pass);
			statement = connection.createStatement();
			resultSet = statement.executeQuery("select* from store where branch = \"" + branchName + "\"");
			int i = 1;
			String[] row;
			row = new String[7];
			while (resultSet.next()) {
				row[0] = Integer.toString(resultSet.getInt(1));
				row[1] = resultSet.getString(2);
				row[2] = resultSet.getString(3);
				row[3] = Double.toString(resultSet.getDouble(4));
				row[4] = Integer.toString(resultSet.getInt(5));
				row[5] = resultSet.getString(6);
				row[6] = resultSet.getString(7);

				tableModel.addRow(row);
				reportTableModel.addRow(row);
				i = resultSet.getInt(1);
			}

			counter = i + 1;
		} catch (SQLException ex) {
			ex.printStackTrace();
		} finally {
			try {
				statement.close();
				connection.close();
				resultSet.close();
			} catch (SQLException v) {
				v.printStackTrace();
			}
		}
	}

	public void search(String portion) {
		try {
			int y = table.getRowCount();
			for (int i = 0; i < y; i++) {
				tableModel.removeRow(0);
			}
			connection = DriverManager.getConnection(url, user, pass);
			statement = connection.createStatement();
			resultSet = statement.executeQuery(
					"select* from store where productName like '" + portion + "%' and branch = \"" + branchName + "\"");
			int i = 1;
			String[] row;
			row = new String[7];
			while (resultSet.next()) {
				row[0] = Integer.toString(resultSet.getInt(1));
				row[1] = resultSet.getString(2);
				row[2] = resultSet.getString(3);
				row[3] = Double.toString(resultSet.getDouble(4));
				row[4] = Integer.toString(resultSet.getInt(5));
				row[5] = resultSet.getString(6);
				row[6] = resultSet.getString(7);

				tableModel.addRow(row);
				i = resultSet.getInt(1);
			}

		} catch (SQLException ex) {
			Logger.getLogger(SupermarketSales.class.getName()).log(Level.SEVERE, null, ex);
		} finally {
			try {
				statement.close();
				connection.close();
				resultSet.close();
			} catch (SQLException v) {
			}
		}
	}

}
