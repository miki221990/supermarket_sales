import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.Font;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.GridLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.FocusEvent;
import java.awt.event.FocusListener;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JPasswordField;
import javax.swing.JScrollPane;
import javax.swing.JTextField;
import javax.swing.SwingUtilities;
import javax.swing.UIManager;

public class SignInSales extends JFrame {

	ImageIcon log = new ImageIcon(getClass().getResource("Asset 12.png"));

	GridBagConstraints loggbc = new GridBagConstraints();
	GridBagLayout gridBag = new GridBagLayout();

	Boolean userCheck = true;
	String userTable;

	JPanel mainPanel = new JPanel(new BorderLayout());
	JPanel logInMainPanel = new JPanel(new BorderLayout());
	JPanel sidePanel = new JPanel(new BorderLayout());
	JPanel imgPanel = new JPanel(new BorderLayout());
	JPanel logInPanel = new JPanel();
	JPanel signUpButtonPanel = new JPanel(new FlowLayout());
	JPanel signInPanel = new JPanel();
	JPanel signInButtonPanel = new JPanel(new FlowLayout());

	JLabel imgLogo = new JLabel(log);
	JLabel userName = new JLabel("User Name");
	JLabel password = new JLabel("Password");
	JLabel firstName = new JLabel("First Name");
	JLabel lastName = new JLabel("Last Name");
	JLabel signPassword = new JLabel("Password");
	JLabel confirmPassword = new JLabel("Confim Password");
	JLabel fillUserName = new JLabel();
	JLabel fillPassword = new JLabel();
	JLabel fillFirstName = new JLabel();
	JLabel fillLastName = new JLabel();
	JLabel fillSignPassword = new JLabel();
	JLabel confirm = new JLabel();
	
	
	JButton logIn = new JButton("Log In");
	JButton signUpButton = new JButton("Sign Up");
	JButton signIn = new JButton("Sign Up");
	JButton back = new JButton("<");

	JTextField userNameTf = new JTextField("Enter Your User Name", 25);
	JPasswordField logPasswordTf = new JPasswordField("Password", 25);
	JTextField firstNameTf = new JTextField("Enter Your First Name", 20);
	JTextField lastNameTf = new JTextField("Enter Your Last Name", 20);
	JPasswordField signPasswordTf = new JPasswordField("Password", 20);
	JPasswordField confirmPasswordTf = new JPasswordField("Password", 20);

	Connection connection = null;
	Statement statement = null;
	ResultSet resultSet = null;

	String host ="NHATTY";
	String port="3306";
	String pass="mikiyas123";
	String dbname="supermarket";
	String name="root";
	String URL="jdbc:mysql://"+host+":"+port+"/"+dbname+"?useSSL=false";

	Font font = new Font("Montseerat", Font.BOLD, 14);
	Font fontPlain = new Font("Montseerat", Font.PLAIN, 12);

	Color blue = new Color(204, 251, 246);
	Color white = new Color(255, 255, 255);
	Color fade = new Color(235, 253, 251);

	private class FocusHandler implements FocusListener {
		@Override
		public void focusGained(FocusEvent fo) {
			fo.getComponent().setForeground(Color.black);
			if (fo.getSource() == userNameTf) {
				if (userNameTf.getText().equals("Enter Your User Name")) {
					userNameTf.setText("");
				}
			} else if (fo.getSource() == logPasswordTf) {
				if (logPasswordTf.getText().equals("Password")) {
					logPasswordTf.setText("");
				}
			} else if (fo.getSource() == firstNameTf) {
				if (firstNameTf.getText().equals("Enter Your First Name")) {
					firstNameTf.setText("");
				}
			} else if (fo.getSource() == lastNameTf) {
				if (lastNameTf.getText().equals("Enter Your Last Name")) {
					lastNameTf.setText("");
				}
			} else if (fo.getSource() == signPasswordTf) {
				if (signPasswordTf.getText().equals("Password")) {
					signPasswordTf.setText("");
				}
			} else if (fo.getSource() == confirmPasswordTf) {
				if (confirmPasswordTf.getText().equals("Password")) {
					confirmPasswordTf.setText("");
				}
			}
		}

		@Override
		public void focusLost(FocusEvent fe) {
			fe.getComponent().setForeground(Color.LIGHT_GRAY);
			if (fe.getSource() == userNameTf) {
				if (userNameTf.getText().equals(""))
					userNameTf.setText("Enter Your User Name");
			} else if (fe.getSource() == logPasswordTf) {
				if (logPasswordTf.getText().equals(""))
					logPasswordTf.setText("Password");
			} else if (fe.getSource() == firstNameTf) {
				if (firstNameTf.getText().equals(""))
					firstNameTf.setText("Enter Your First Name");
			} else if (fe.getSource() == lastNameTf) {
				if (lastNameTf.getText().equals(""))
					lastNameTf.setText("Enter Your Last Name");
			} else if (fe.getSource() == signPasswordTf) {
				if (signPasswordTf.getText().equals(""))
					signPasswordTf.setText("Password");
			} else if (fe.getSource() == confirmPasswordTf) {
				if (confirmPasswordTf.getText().equals(""))
					confirmPasswordTf.setText("Password");
			}

		}
	}

	private class ActionHandler implements ActionListener {
		@Override
		public void actionPerformed(ActionEvent e) {
			if (e.getSource() == logIn) {
				if (userNameTf.getText().equals("Enter Your User Name")) {
					fillUserName.setText("Please Enter User Name!!");
				}
				if (logPasswordTf.getText().equals("Enter Your Password")) {
					fillPassword.setText("Please Enter Password!!");
				}
				try {
					connection = DriverManager.getConnection(URL, name, pass);
					statement = connection.createStatement();
					resultSet = statement.executeQuery("select* from users");
					while (resultSet.next()) {
						if (userNameTf.getText().equals(resultSet.getString(2))) {
							if (logPasswordTf.getText().equals(resultSet.getString(3))) {
								String bran = resultSet.getString(4);
								SupermarketSales supermarketSales = new SupermarketSales(bran);
								supermarketSales.setVisible(true);
								userTable=userNameTf.getText();
								setVisible(false);
								userCheck = true;
								break;
							}
						} else {
							userCheck = false;
						}
					}
					if (userCheck == false) {
						JOptionPane.showMessageDialog(null, "Wrong Username or Password \n\nSign Up or Retry Log In");
					}
				} catch (Exception exe) {
					exe.printStackTrace();
				}

			}
			if (e.getSource() == signUpButton) {
				mainPanel.add(signInPanel);
				logInMainPanel.setVisible(false);
				signInPanel.setVisible(true);
			} else if (e.getSource() == back) {
				signInPanel.setVisible(false);
				logInMainPanel.setVisible(true);
			} else if (e.getSource() == signIn) {
				if (firstNameTf.getText().equals("Enter Your First Name")) {
					fillFirstName.setText("Please Enter Your First Name!!");
				}
				if (lastNameTf.getText().equals("Enter Your Last Name")) {
					fillLastName.setText("Please Enter Your Last Name!!");
				}
				if (signPasswordTf.getText().equals("Password")) {
					fillPassword.setText("Please Enter a valid Password");
				}
				if (!(confirmPasswordTf.getText().equals(signPasswordTf.getText()))) {
					confirm.setText("Your PassWord Doesn't Match!!");
					confirmPasswordTf.setText("");
				}
				if(!(firstNameTf.getText().equals("Enter Your First Name"))&&!(lastNameTf.getText().equals("Enter Your Last Name"))&&!(signPasswordTf.getText().equals("Password"))&&confirmPasswordTf.getText().equals(signPasswordTf.getText())) {
					logInMainPanel.setVisible(true);
					signInPanel.setVisible(false);
					JOptionPane.showMessageDialog(null, "You have Successfuly Signed Up \n\nYour Username is : "
							+ firstNameTf.getText() + "" + lastNameTf.getText() + "\nThanks");
					try {
						connection = DriverManager.getConnection(URL, name, pass);
						statement = connection.createStatement();
						statement.execute("insert into users values('" + firstNameTf.getText() + lastNameTf.getText()
								+ "','" + signPasswordTf.getText() + "')");
						
					} catch (SQLException e1) {
						// TODO Auto-generated catch block
						e1.printStackTrace();
					}
				}	
			}
		}
	}

	public void addComponent(Component component, int x, int y) {
		loggbc.insets = new Insets(20, 0, 5, 5);
		
		loggbc.gridx = x;
		loggbc.gridy = y;
		loggbc.weightx = 1;
		loggbc.fill = GridBagConstraints.NONE;
		loggbc.anchor = GridBagConstraints.NORTHWEST;
		logInPanel.setLayout(gridBag);
		logInPanel.add(component, loggbc);
	}

	public void addComponentToSign(Component component, int x, int y) {
		loggbc.insets = new Insets(10, 0, 5, 5);
		loggbc.gridx = x;
		loggbc.gridy = y;
		loggbc.weightx = 1;
		loggbc.fill = GridBagConstraints.NONE;
		loggbc.anchor = GridBagConstraints.NORTHWEST;
		signInPanel.setLayout(gridBag);
		signInPanel.add(component, loggbc);
	} 
	

	SignInSales() {

		UIManager.LookAndFeelInfo[] looks = UIManager.getInstalledLookAndFeels();
		try {
			UIManager.setLookAndFeel(looks[1].getClassName());
			SwingUtilities.updateComponentTreeUI(mainPanel);
			SwingUtilities.updateComponentTreeUI(userNameTf);
			SwingUtilities.updateComponentTreeUI(logPasswordTf);
			SwingUtilities.updateComponentTreeUI(signUpButton);
			SwingUtilities.updateComponentTreeUI(logIn);
			SwingUtilities.updateComponentTreeUI(firstNameTf);
			SwingUtilities.updateComponentTreeUI(lastNameTf);
			SwingUtilities.updateComponentTreeUI(signPasswordTf);
			SwingUtilities.updateComponentTreeUI(confirmPasswordTf);
			SwingUtilities.updateComponentTreeUI(signIn);

		} catch (Exception e) {
			e.printStackTrace();
		}

		// Objects

		ActionHandler actionHandler = new ActionHandler();
		FocusHandler focusHandler = new FocusHandler();

		// redColors

		fillUserName.setForeground(Color.red);
		fillFirstName.setForeground(Color.red);
		fillLastName.setForeground(Color.red);
		fillPassword.setForeground(Color.red);
		fillSignPassword.setForeground(Color.red);
		confirm.setForeground(Color.RED);

		// SidePanel

		sidePanel.setBackground(blue);
		imgPanel.setBackground(blue);
		signUpButtonPanel.setBackground(blue);
		imgPanel.add(imgLogo, BorderLayout.CENTER);
		signUpButton.setFont(font);
		signUpButton.setFocusPainted(false);
		signUpButton.setPreferredSize(new Dimension(90, 30));
		// signUpButtonPanel.add(signUpButton);
		sidePanel.add(imgPanel, BorderLayout.NORTH);
		sidePanel.add(signUpButtonPanel);

		// LogInPanel

		userNameTf.setForeground(Color.LIGHT_GRAY);
		logPasswordTf.setForeground(Color.LIGHT_GRAY);
		logInPanel.setBackground(white);
		userName.setFont(font);
		password.setFont(font);
		logIn.setFont(font);
		addComponent(userName, 1, 1);
		addComponent(userNameTf, 2, 1);
		addComponent(fillUserName, 1, 2);
		addComponent(password, 1, 3);
		addComponent(logPasswordTf, 2, 3);
		addComponent(fillPassword, 1, 4);
		logIn.setFocusPainted(false);
		addComponent(logIn, 2, 5);
		logIn.addActionListener(actionHandler);
		signUpButton.addActionListener(actionHandler);
		userNameTf.addFocusListener(focusHandler);
		logPasswordTf.addFocusListener(focusHandler);

		// logInMainPanel

		logInMainPanel.add(sidePanel, BorderLayout.WEST);
		logInMainPanel.add(logInPanel, BorderLayout.CENTER);

		// SigninPanel

		addComponentToSign(firstName, 1, 1);
		addComponentToSign(firstNameTf, 2, 1);
		addComponentToSign(fillFirstName, 1, 2);
		addComponentToSign(lastName, 1, 3);
		addComponentToSign(lastNameTf, 2, 3);
		addComponentToSign(fillLastName, 1, 4);
		addComponentToSign(signPassword, 1, 5);
		addComponentToSign(signPasswordTf, 2, 5);
		addComponentToSign(fillSignPassword, 1, 6);
		addComponentToSign(confirmPassword, 1, 7);
		addComponentToSign(confirmPasswordTf, 2, 7);
		addComponentToSign(confirm, 1, 8);
		addComponentToSign(signIn, 1, 9);
		addComponentToSign(back, 3, 9);
		firstNameTf.setForeground(Color.LIGHT_GRAY);
		lastNameTf.setForeground(Color.LIGHT_GRAY);
		signPasswordTf.setForeground(Color.LIGHT_GRAY);
		confirmPasswordTf.setForeground(Color.LIGHT_GRAY);
		signInPanel.setBackground(white);
		firstName.setFont(font);
		fillFirstName.setFont(fontPlain);
		lastName.setFont(font);
		fillLastName.setFont(fontPlain);
		signPassword.setFont(font);
		fillSignPassword.setFont(fontPlain);
		confirmPassword.setFont(font);
		confirm.setFont(fontPlain);
		signIn.setFont(font);
		back.setFont(font);
		firstNameTf.addActionListener(actionHandler);
		lastNameTf.addActionListener(actionHandler);
		signPasswordTf.addActionListener(actionHandler);
		confirmPasswordTf.addActionListener(actionHandler);
		signIn.addActionListener(actionHandler);
		firstNameTf.addFocusListener(focusHandler);
		lastNameTf.addFocusListener(focusHandler);
		signPasswordTf.addFocusListener(focusHandler);
		confirmPasswordTf.addFocusListener(focusHandler);
		back.addActionListener(actionHandler);

		// mainPanel

		mainPanel.add(logInMainPanel);

		// frame

		add(mainPanel);
		setResizable(false);
	}

}
